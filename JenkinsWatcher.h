//
// Created by steven on 20/02/19.
//

#ifndef BTJENKINSLIGHT_JENKINSWATCHER_H
#define BTJENKINSLIGHT_JENKINSWATCHER_H


#include <QtCore/QString>
#include <QtCore/QUrl>
#include <QtCore/QUrlQuery>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtCore/QSettings>
#include <QtCore/QEventLoop>
#include <QtNetwork/QAuthenticator>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonArray>

class JenkinsWatcher : public QObject{
    Q_OBJECT
public :
    enum BUILD_STATE {
        RED,BLUE,YELLOW,RUNNING_RED,RUNNING_BLUE,RUNNING_YELLOW
    };

    JenkinsWatcher(QString root,QString login ="", QString password ="") :
            _RootURL( root +"/api/json"),_Login(login),_Password(password)
    {
        _RootURL.setUserName(_Login);
        _RootURL.setPassword(_Password);
    }

    QNetworkRequest CreateRequest(QUrl const & inputurl) const
    {

        QUrl url = inputurl;
        QUrlQuery query;
        query.addQueryItem("token",_Password);
        url.setQuery(query);

        QNetworkRequest request(url);

        QString headerData = QString("%1:%2").arg(_Login).arg(_Password).toUtf8().toBase64();
        request.setRawHeader("Authorization","Basic "+headerData.toUtf8());
        return request;
    }

    QByteArray Get(const QUrl & url) const {

        QEventLoop loop;

        auto reply = _NetworkManager.get(CreateRequest(url));

        connect(reply,&QNetworkReply::finished,[&reply,&loop](){
            loop.quit();
            reply->deleteLater();
        });

        loop.exec();
        return  reply->readAll();
    }
    QString getJobAttr(QString jobName, QString attr) const
    {
        auto json = QJsonDocument::fromJson(Get(_RootURL));
        auto jobs = json["jobs"];

        for(const auto & j : jobs.toArray() )
        {
            auto job  = j.toVariant().toMap() ;
            auto name = job["name"].toString();
            if(name == jobName)
                return job[attr].toString();
        }

        return "";
    }

protected :

    QUrl _RootURL;
    QString _Login;
    QString _Password;
    mutable QNetworkAccessManager _NetworkManager;
    QSettings _Settings;
};


#endif //BTJENKINSLIGHT_JENKINSWATCHER_H
