//
// Created by steven on 17/02/19.
//

#include "ConsoleReader.h"
#include <unistd.h> //Provides STDIN_FILENO
#include <QTextStream>
ConsoleReader::ConsoleReader(QObject *parent) :
        QObject(parent),
        notifier(STDIN_FILENO, QSocketNotifier::Read)
{
    connect(&notifier, SIGNAL(activated(int)), this, SLOT(text()));
}

void ConsoleReader::text()
{
    QTextStream qin(stdin);
    QString line = qin.readLine();
    emit textReceived(line);
}