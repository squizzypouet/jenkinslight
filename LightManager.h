//
// Created by steven on 17/02/19.
//

#ifndef BTJENKINSLIGHT_BTMANAGER_H
#define BTJENKINSLIGHT_BTMANAGER_H


#include <QtCore/QObject>
#include <QtBluetooth/QBluetoothDeviceDiscoveryAgent>
#include <QDebug>
#include <iostream>
#include <QtBluetooth/QBluetoothUuid>
#include <QtBluetooth/QBluetoothAddress>
#include <QList>
#include <QtBluetooth/QLowEnergyController>
#include <QtCore/QEventLoop>
#include <QtCore/QTimer>
#include <map>
#include <algorithm>



class LightManager : public QObject {
    Q_OBJECT

static const QBluetoothUuid HM10_CUSTOM_SERVICE ;

public :

    LightManager() {

        m_deviceDiscoveryAgent = new QBluetoothDeviceDiscoveryAgent(this);
        m_deviceDiscoveryAgent->setLowEnergyDiscoveryTimeout(5000);

        connect(m_deviceDiscoveryAgent, &QBluetoothDeviceDiscoveryAgent::deviceDiscovered, [&,this](const QBluetoothDeviceInfo &device)
        {
            if ( ! device.coreConfigurations() & QBluetoothDeviceInfo::LowEnergyCoreConfiguration)
                return ;

            for(auto ctrl_service : m_services)
            {
                auto & controller = ctrl_service.first;
                bool eq_addr = !device.address().isNull()    &&  device.address()    == controller->remoteAddress() ;
                bool eq_uuid = !device.deviceUuid().isNull() &&  device.deviceUuid() == controller->remoteDeviceUuid();

                if( eq_addr || eq_uuid)
                    return ; //Device déjà connu !
            }

            if(device.serviceUuids().contains(HM10_CUSTOM_SERVICE))
            {
                auto controller = QLowEnergyController::createCentral(device,this);

                connect(controller,&QLowEnergyController::connected,[controller,this](){
                    controller->discoverServices();
                });

                connect(controller,&QLowEnergyController::discoveryFinished,[controller,device,this](){
                    for(auto & s : controller->services())
                        if(s == HM10_CUSTOM_SERVICE)
                        {
                            auto service = controller->createServiceObject(s,this) ;
                            service->discoverDetails();
                            this->m_services[controller].append(service);
                            qDebug()<< device.name() <<" "<<device.address() ;
                        }
                });

                controller->connectToDevice();
            }
        });
        connect(m_deviceDiscoveryAgent, static_cast<void (QBluetoothDeviceDiscoveryAgent::*)(QBluetoothDeviceDiscoveryAgent::Error)>(&QBluetoothDeviceDiscoveryAgent::error),[](){std::cout << "SCAN ERROR ..." << std::endl;});

        connect(m_deviceDiscoveryAgent, &QBluetoothDeviceDiscoveryAgent::finished, [&,this](){});

        connect(&m_discover,&QTimer::timeout, [this](){
            if(!m_deviceDiscoveryAgent->isActive())
                m_deviceDiscoveryAgent->start(QBluetoothDeviceDiscoveryAgent::LowEnergyMethod);
        });


        m_discover.start(15000);
        m_deviceDiscoveryAgent->start(QBluetoothDeviceDiscoveryAgent::LowEnergyMethod);
    }

public slots :
    void run(){
        std::cout << m_services.size() << " services discovered" <<std::endl;

        for(auto ctrl_service : m_services)
        {
            const auto & controller = ctrl_service.first;
            if(controller->state() == QLowEnergyController::UnconnectedState)
            {
                controller->disconnect();
                qDebug()<<controller->remoteName() << controller->remoteAddress() << "Lost";
                m_services.erase(controller);
                break;
            }

            const auto & services   = ctrl_service.second;

            for(const auto & s : services)
            {
                for(auto & characteristic : s->characteristics())
                {
                    s->writeCharacteristic(characteristic,_DataToSend,QLowEnergyService::WriteWithoutResponse);
                }
            }
        }

    }



    void send(QByteArray data)
    {
        while(data.size()<10)
            data.append((char)0);

        _DataToSend = data ;
        run();
    };


signals:
    void start();
protected:

    QByteArray _DataToSend;

    QTimer m_discover;
    std::map<QLowEnergyController*,QList<QLowEnergyService *>> m_services;
    QBluetoothDeviceDiscoveryAgent * m_deviceDiscoveryAgent;

};


#endif //BTJENKINSLIGHT_BTMANAGER_H
