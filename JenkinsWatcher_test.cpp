//
// Created by steven on 20/02/19.
//

#include <QtCore/QCoreApplication>
#include <QtCore/QTimer>
#include "gtest/gtest.h"
#include "JenkinsWatcher.h"

TEST(JenkinsWatcherTest, listJobs)
{
    char * argv = "";
    int argc = 1;
    QCoreApplication app(argc,&argv);

    QTimer::singleShot(100,[&](){
        JenkinsWatcher jwatcher("http://localhost:8080","admin","115f980b0ad149e597f4a42405d12f32c2");
        EXPECT_EQ("red",jwatcher.getJobAttr("TEST","color").toStdString());
        app.quit();
    });
    app.exec();
}