#include <iostream>
#include <QtCore/QCoreApplication>
#include <QtCore/QFile>
#include <QtCore/QTimer>
#include <QtCore/QSettings>
#include <QtCore/QCommandLineParser>
#include "LightManager.h"
#include "ConsoleReader.h"
#include "JenkinsWatcher.h"
#include <memory>

std::unique_ptr<QSettings> generateDefaultSettings(const QString &inifile);

int main(int argc, char** argv) {
    QCoreApplication app(argc,argv);

    QCommandLineParser parser;
    parser.setApplicationDescription("BT Jenkins light driver v0.1");
    parser.addHelpOption();
    parser.addOption({{"s","settings"}, "Settings .ini file", "file","./settings.ini"});
    parser.process(app);

    QString inifile = parser.value("settings");

    std::unique_ptr<QSettings> settings;
    if(!QFile(inifile).exists())
    {
        settings = generateDefaultSettings(inifile);
    }
    else
        settings = std::make_unique<QSettings>(inifile, QSettings::IniFormat);



    if(settings->value("URL").toString().isEmpty())
    {
        std::cout << "Invalid URL"<< std::endl;
        return -1;
    }

    auto jenkins    = std::make_unique<JenkinsWatcher>(settings->value("URL").toString(),settings->value("username").toString(),settings->value("token").toString());
    auto finder     = std::make_unique<LightManager>();
    auto ticker     = std::make_unique<QTimer>();
    auto lastState  = std::make_unique<QByteArray>("-");
    auto lastUpdate = std::chrono::system_clock::now();

    app.connect(ticker.get(),&QTimer::timeout, [&settings,&jenkins,&finder,&lastUpdate,&lastState](){

        auto job_color = jenkins->getJobAttr(settings->value("job").toString(),"color");

        bool blink = job_color.contains("_anime") ;
        char r=0, g=0, b=0, w=0;
        if(job_color.contains("red"))
        {
            r=255 ;
        }
        else if(job_color.contains("blue"))
        {
            g=255; b=255;
        }
        else if (job_color.contains("yellow"))
        {
            r=255; g=64; w=64;
        }

        QByteArray data("STATE") ;
        data.append(r).append(g).append(b).append(w).append((char)blink) ;

        auto broadcastEvery = settings->value("broadcastEvery",30);
        bool force_update = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now() - lastUpdate).count() > 30 ;
        if(force_update || data != *lastState)
        {
            qDebug()<< data;
            finder->send(data);
            *lastState = data;
            lastUpdate = std::chrono::system_clock::now();
        }

    });

    ticker->start(std::max(1,settings->value("pullJenkinsEvery",2).toInt()) * 1000);

    return app.exec();
}

std::unique_ptr<QSettings> generateDefaultSettings(const QString &inifile) {
    auto settings = std::make_unique<QSettings>(inifile, QSettings::IniFormat);
    settings->setValue("URL","http://localhost:8080");
    settings->setValue("job","trunk");
    settings->setValue("username","admin");
    settings->setValue("token","115f980b0ad149e597f4a42405d12f32c2");
    settings->setValue("pullJenkinsEvery",2);
    settings->setValue("broadcastEvery",30);
    settings->sync();
    return std::move(settings);
}
