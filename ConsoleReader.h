//
// Created by steven on 17/02/19.
//

#ifndef BTSERIAL_CONSOLEREADER_H
#define BTSERIAL_CONSOLEREADER_H

#include <QObject>
#include <QSocketNotifier>

class ConsoleReader : public QObject
{
Q_OBJECT
public:
    explicit ConsoleReader(QObject *parent = 0);
signals:
    void textReceived(QString message);
public slots:
    void text();
private:
    QSocketNotifier notifier;
};

#endif //BTSERIAL_CONSOLEREADER_H
